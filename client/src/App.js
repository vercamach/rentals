import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { createBrowserHistory } from "history";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import Landing from './components/LandingPage';
import Login from './components/Login';
import Signup from './components/Signup';
import UploadPostFacilities from './components/UploadPostFacilities';
import UploadPostDescription from './components/UploadPostDescription';
import Wishlist from './components/Wishlist';
import ListedHomes from './components/ListedHomes';
import ListedHomeDetail from './components/ListedHomeDetail';
import HousesOverview from './components/HousesOverview';
import BookingOverview from './components/BookingOverview';
import Payment from './components/Payment';
import PurchaseConfirmation from './components/PurchaseConfirmation';

function App() {
  const history = createBrowserHistory();
  const localStorageEmail = localStorage.getItem('email');
  const authenticatedNavLinks = [
    {
      href: "/facilities", text: "List your home"
    },
    {
      href: "/wishlist", text: "Saved homes"
    },
    {
      href: "/my-homes", text: "Listed homes"
    },
    {
      href: "/logout", text: "Log out", logout: true
    },
  ]

  const initialNavLinks = [
    {
      href: "/signup", text: "Sign up"
    },
    {
      href: "/login", text: "Log in"
    }
  ]
  const handleLogout = history => () => {
    history.push('/login');
    localStorage.removeItem('email');
  };
  return (
    <div className="App">
      <Router history={history}>
        <div className="navbar-brand">
          <Navbar.Brand href="/">
            <img
              src={require("./imgs/home.png")}
              width="25"
              height="25"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Brand href="/">HOLIDAYNORD</Navbar.Brand>
        </div>
        <Navbar bg="dark" variant="dark" className="justify-content-end">

          <Nav activeKey={window.location.pathname} >
            {localStorageEmail === null ?
              initialNavLinks.map((link, i) => < Nav.Link href={link.href} key={i}> {link.text}</Nav.Link>) :
              authenticatedNavLinks.map((link, i) => < Nav.Link href={link.href} key={i} onClick={link.logout && handleLogout(history)}> {link.text}</Nav.Link>)
            }
          </Nav>
        </Navbar>
        <div >
          <Switch>
            <Route exact path="/" component={props => <Landing {...props} />} />
            <Route exact path="/login" component={props => <Login {...props} />} />
            <Route exact path="/signup" component={props => <Signup {...props} />} />
            <Route exact path="/facilities" component={props => <UploadPostFacilities {...props} />} />
            <Route exact path="/description" component={props => <UploadPostDescription {...props} />} />
            <Route exact path="/wishlist" component={props => <Wishlist {...props} />} />
            <Route exact path="/my-homes" component={props => <ListedHomes {...props} />} />
            <Route exact path="/listed-home-detail" component={props => <ListedHomeDetail {...props} />} />
            <Route exact path="/houses-overview/*" component={props => <HousesOverview {...props} />} />
            <Route exact path="/booking-overview" component={props => <BookingOverview {...props} />} />
            <Route exact path="/payment" component={props => <Payment {...props} />} />
            <Route exact path="/purchase-confirmation" component={props => <PurchaseConfirmation {...props} />} />
            <Redirect from='/logout' to='/' />
          </Switch>
        </div>
      </Router>
    </div >
  );
}

export default App;
