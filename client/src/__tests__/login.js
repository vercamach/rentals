import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Login from "../components/Login";

test("allows the user to login successfully", async () => {
  // mock out window.fetch for the test
  const fakeUserResponse = { token: "fake_user_token" };
  jest.spyOn(window, "fetch").mockImplementationOnce(() => {
    return Promise.resolve({
      json: () => Promise.resolve(fakeUserResponse)
    });
  });

  const { getByLabelText, getByText, findByRole } = render(<Login />);

  // fill out the form
  fireEvent.change(getByLabelText(/email address/i), {
    target: { value: "gudanedita@gmail.com" }
  });
  fireEvent.change(getByLabelText(/password/i), {
    target: { value: "password" }
  });

  fireEvent.click(
    getByText(/log in/i, {
      selector: "button"
    })
  );

  expect(fetch).toHaveBeenCalledWith("http://localhost:8080/users/login", {
    headers: {
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({
      email: "gudanedita@gmail.com",
      password: "password"
    })
  });
});
