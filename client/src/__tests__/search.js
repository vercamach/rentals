import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, fireEvent, waitForElement } from "@testing-library/react";
import App from "../App";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

function renderWithRouter(
  ui,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) {
  const Wrapper = ({ children }) => (
    <Router history={history}>{children}</Router>
  );
  return {
    ...render(ui, { wrapper: Wrapper }),
    history
  };
}

test("the search form allow user seccessfully complete a form", async () => {
  const route = "/";
  const {
    getByLabelText,
    getByText,
    findByRole,
    debug
  } = renderWithRouter(<App />, { route });

  // fill out the form
  fireEvent.change(getByLabelText(/location/i), {
    target: { value: "sweden" }
  });
  fireEvent.change(getByLabelText(/arrival/i), {
    target: { value: "31/12/2020" }
  });
  fireEvent.change(getByLabelText(/departure/i), {
    target: { value: "05/01/2021" }
  });
  const button = getByText(/search/i);
  fireEvent.click(button);

  await waitForElement(() => getByText(/Sweden/i, { selector: "h3" }));
});
