import "@testing-library/jest-dom/extend-expect";
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Signup from "../components/Signup";

test("allows the user to signup successfully", async () => {
  // mock out window.fetch for the test
  const fakeUserResponse = { token: "fake_user_token" };
  jest.spyOn(window, "fetch").mockImplementationOnce(() => {
    return Promise.resolve({
      json: () => Promise.resolve(fakeUserResponse)
    });
  });
  const { getByLabelText, getByText, findByRole } = render(<Signup />);

  // fill out the form
  fireEvent.change(getByLabelText(/username/i), { target: { value: "Edita" } });
  fireEvent.change(getByLabelText(/email/i), {
    target: { value: "gudanedita@gmail.com" }
  });
  fireEvent.change(getByLabelText("Password"), {
    target: { value: "password" }
  });
  fireEvent.change(getByLabelText(/repeat password/i), {
    target: { value: "password" }
  });
  fireEvent.click(getByText(/register/i));

  expect(fetch).toHaveBeenCalledWith("http://localhost:8080/users/register", {
    headers: {
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({
      username: "Edita",
      email: "gudanedita@gmail.com",
      password: "password",
      repeatPassword: "password"
    })
  });
});
