import React from "react";
import { Modal } from "./elements";
import Carousel from "react-bootstrap/Carousel";

export default function ListedHomeDetail(props) {
  const localStorageEmail = localStorage.getItem("email");

  localStorageEmail === null && props.history.push("/login");
  const [open, setOpen] = React.useState(false);

  const [fullDescriptionClosed, setFullDescriptionClosed] = React.useState(
    true
  );
  const guestsAmount = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const reviewsStars = [2, 3, 4, 5];
  const [savedHouse, setSavedHouse] = React.useState(false);
  const utilitiesIntro = [
    {
      icon: require("../imgs/house-icon.png"),
      alt: "house-icon",
      subtitleBold: "A whole house",
      subtitle: "You will have the whole place to yourself"
    },
    {
      icon: require("../imgs/cleaning-icon.png"),
      alt: "cleaning-icon",
      subtitleBold: "Super clean",
      subtitle: "Guests are saying this place is super clean"
    },
    {
      icon: require("../imgs/pet-icon.png"),
      alt: "pet-icon",
      subtitleBold: "Pets allowed",
      subtitle: "You are allowed to have pets in this house"
    },
    {
      icon: require("../imgs/parking-icon.png"),
      alt: "parking-icon",
      subtitleBold: "Free parking",
      subtitle: "You can park for free in front of the house"
    }
  ];

  const facilitiesListLeft = [
    { img: require("../imgs/wifi-icon.png"), text: "Wi fi" },
    { img: require("../imgs/parking-icon2.png"), text: "Free parking" },
    { img: require("../imgs/heat-icon.png"), text: "Heater" },
    { img: require("../imgs/wash-mach-icon.png"), text: "Washing machine" }
  ];
  const facilitiesListRight = [
    { img: require("../imgs/baby-bed-icon.png"), text: "Baby bed" },

    { img: require("../imgs/hair-dryer-icon.png"), text: "Hair dryer" },
    { img: require("../imgs/cosmetic-icon.png"), text: "Basics" },
    { img: require("../imgs/wash-mach-icon.png"), text: "Washing machine" }
  ];

  const reviews = [
    {
      name: "Antonio",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    },
    {
      name: "Emily",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    },
    {
      name: "Sara",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    },
    {
      name: "Edita",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    },
    {
      name: "Christian",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    },
    {
      name: "Peter",
      text:
        "Tiramisu cake carrot cake powder lollipop. Wafer gingerbread sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet macaroon pudding marzipan tiramisu cupcake. Donut jelly beans wafer bonbon bear claw tiramisu. Danish chocolate cake sugar plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet cheesecake tootsie roll."
    }
  ];
  const carouselImgs = [
    { src: require("../imgs/house.jpg") },
    { src: require("../imgs/room.jpg") },
    { src: require("../imgs/room1.jpg") },
    { src: require("../imgs/room2.jpg") },
    { src: require("../imgs/room3.jpg") },
    { src: require("../imgs/room4.jpg") },
    { src: require("../imgs/room5.jpg") }
  ];

  return (
    <div>
      <Modal
        modalStyle={{ maxWidth: 900 }}
        isOpen={open}
        onClose={() => {
          setOpen(false);
        }}
      >
        <Carousel>
          {carouselImgs.map((img, i) => (
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={img.src}
                alt={i}
                height="600px"
              />
            </Carousel.Item>
          ))}
        </Carousel>
      </Modal>

      <div className="flex-container overflow-hidden">
        <div style={{ overflow: "hidden" }}>
          <img
            src={require("../imgs/house.jpg")}
            alt="img"
            className="transition"
            height="440px"
            width="auto"
            onClick={() => setOpen(true)}
          />
        </div>
        <div>
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room1.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
        </div>

        <div className="overflow-hidden">
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room2.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room3.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
        </div>
        <div className="overflow-hidden">
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room4.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
          <div className="overflow-hidden">
            <img
              src={require("../imgs/room5.jpg")}
              alt="img"
              className="single-home-img transition"
              onClick={() => setOpen(true)}
            />
          </div>
        </div>
      </div>
      <div className="single-house-container">
        <div className="margin-top" style={{ width: 500 }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            {" "}
            <h2>Charming house next to a nature</h2>
            <div
              onClick={() => setSavedHouse(!savedHouse)}
              style={{ cursor: "pointer" }}
            >
              <svg
                style={{ height: 25, marginBottom: "0.5rem" }}
                version="1.1"
                id="Capa_1"
                x="0px"
                y="0px"
                viewBox="0 0 580 540"
                enable-background="new 0 0 580 540"
              >
                <path
                  fill={savedHouse ? "#D75A4A " : "#C8C8C8"}
                  d="M288.326,103.411c22.602-53.57,74.234-91,134.288-91c80.898,0,139.16,69.205,146.485,151.682
	c0,0,3.954,20.474-4.749,57.333c-11.85,50.198-39.704,94.797-77.258,128.834L288.326,527.6L92.908,350.248
	C55.354,316.222,27.5,271.613,15.65,221.414c-8.702-36.859-4.749-57.333-4.749-57.333C18.227,81.605,76.489,12.4,157.387,12.4
	C217.452,12.4,265.724,49.842,288.326,103.411z"
                />
              </svg>
            </div>{" "}
          </div>
          <p className="text-thin">Copenhagen</p>
          <div className="flex">
            <p>4 guests</p>
            <p>2 bedrooms</p>
            <p>3 beds</p>
            <p>1 bathroom</p>
          </div>
          <hr />
          {utilitiesIntro.map((utility) => (
            <div>
              <div className="flex-container margin-top">
                <img src={utility.icon} alt={utility.alt} className="icon" />
                <p className="subtitle-bold">{utility.subtitleBold}</p>
              </div>
              <div className="flex">
                <p>{utility.subtitle}</p>
              </div>
            </div>
          ))}
          <hr />
          <p className="subtitle-bold">Description</p>
          {fullDescriptionClosed ? (
            <div>
              <p>
                Gummies pastry candy canes topping croissant fruitcake lemon
                drops icing powder. Chocolate cake tootsie roll macaroon.
                Dessert fruitcake jelly. Gummi bears sugar plum biscuit pie
                wafer topping bear claw. Powder muffin biscuit. Jelly beans
                chocolate cake jelly. Apple pie halvah chocolate bar fruitcake
                candy canes gummi bears oat cake candy cake. Toffee dessert
                sweet bear claw cupcake. Powder tart cookie chocolate bar
                dessert bear claw cheesecake...
              </p>
              <p
                style={{
                  cursor: "pointer",
                  textDecoration: "underline",
                  color: "#343a40"
                }}
                className="subtitle-bold"
                onClick={() => setFullDescriptionClosed(false)}
              >
                Read more{" "}
              </p>
            </div>
          ) : (
            <div>
              <p>
                Gummies pastry candy canes topping croissant fruitcake lemon
                drops icing powder. Chocolate cake tootsie roll macaroon.
                Dessert fruitcake jelly. Gummi bears sugar plum biscuit pie
                wafer topping bear claw. Powder muffin biscuit. Jelly beans
                chocolate cake jelly. Apple pie halvah chocolate bar fruitcake
                candy canes gummi bears oat cake candy cake. Toffee dessert
                sweet bear claw cupcake. Powder tart cookie chocolate bar
                dessert bear claw cheesecake muffin toffee. Brownie powder tart
                carrot cake candy sweet roll dessert. Powder tiramisu chocolate
                ice cream sesame snaps carrot cake liquorice. Toffee pudding
                biscuit wafer candy gummies candy caramels.
              </p>
              <p>
                Chocolate cake dessert soufflé donut carrot cake marzipan
                soufflé. Muffin tootsie roll macaroon chocolate topping sesame
                snaps chocolate cake carrot cake. Halvah cake chocolate cookie.
                Jelly-o tiramisu sweet roll lollipop cake lollipop carrot cake
                candy. Jelly-o jelly tootsie roll jujubes soufflé tart lollipop
                wafer jujubes. Tootsie roll bear claw jujubes gummi bears
                dessert tart. Pastry bear claw chocolate cake. Tiramisu tiramisu
                apple pie soufflé toffee. Macaroon donut caramels apple pie
                macaroon toffee marzipan oat cake. Gingerbread tootsie roll
                soufflé. Soufflé sesame snaps pudding danish muffin carrot cake
                toffee muffin macaroon. Wafer ice cream dragée gummies apple
                pie. Gingerbread gingerbread jujubes chocolate cake icing
                gummies bear claw powder. Cookie jelly-o tart sesame snaps ice
                cream halvah fruitcake apple pie.
              </p>
              <p>
                Muffin marzipan jujubes candy canes jelly. Wafer powder cake
                chocolate chocolate cake gummies. Sugar plum cake gummi bears
                brownie croissant jelly beans. Tootsie roll candy gummi bears
                brownie jelly. Chocolate cake brownie soufflé. Powder candy cake
                oat cake jelly-o chocolate bar. Toffee chocolate cake apple pie
                lemon drops jelly beans. Biscuit bonbon biscuit icing gummies
                tart candy. Candy canes cheesecake icing lemon drops chupa chups
                wafer dragée fruitcake. Cotton candy jelly beans bonbon cake
                cake croissant cake. Candy canes ice cream powder chocolate cake
                icing. Muffin jujubes gummies wafer cake. Chocolate cake carrot
                cake dragée croissant toffee candy canes. Chocolate oat cake
                topping jelly beans tootsie roll candy canes sweet jelly beans.
              </p>
              <p>
                Tiramisu cake carrot cake powder lollipop. Wafer gingerbread
                sugar plum apple pie jelly. Sweet roll fruitcake candy. Sweet
                macaroon pudding marzipan tiramisu cupcake. Donut jelly beans
                wafer bonbon bear claw tiramisu. Danish chocolate cake sugar
                plum muffin. Halvah pastry dragée icing cupcake tiramisu sweet
                cheesecake tootsie roll. Jelly beans biscuit lemon drops muffin.
                Chocolate bar cake danish. Liquorice ice cream cheesecake
                jujubes tootsie roll. Chupa chups chocolate cake macaroon apple
                pie dragée caramels bear claw bonbon. Candy canes chocolate
                jelly beans gummies croissant danish chocolate cake dessert.
              </p>
            </div>
          )}
          <hr />
          <div>
            <p className="subtitle-bold">Facilities</p>
            <div style={{ display: "flex", justifyContent: "space-evenly" }}>
              <div>
                {facilitiesListLeft.map((item) => (
                  <div className="flex-container margin-top">
                    <img
                      src={item.img}
                      alt="icon"
                      height="20px"
                      style={{ marginRight: 15 }}
                    />
                    <p>{item.text}</p>
                  </div>
                ))}
              </div>
              <div>
                {facilitiesListRight.map((item) => (
                  <div className="flex-container margin-top">
                    <img
                      src={item.img}
                      alt="icon"
                      height="20px"
                      style={{ marginRight: 15 }}
                    />
                    <p>{item.text}</p>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <hr />
          <div>
            <p className="subtitle-bold">Reviews</p>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "baseline"
              }}
            >
              <p>
                <img
                  src={require("../imgs/star-icon.png")}
                  alt="icon"
                  height="15px"
                  style={{ marginBottom: 5 }}
                />{" "}
                4,76
              </p>
              <p>435 reviews</p>
              <select style={{ width: "50%" }}>
                <option disabled selected value="dilter">
                  Filter
                </option>
                <option value="1 star">1 star</option>
                {reviewsStars.map((star) => (
                  <option value={star + " stars"}>{star + " stars"}</option>
                ))}
              </select>
            </div>
            <hr />
            <div>
              {reviews.map((review) => (
                <>
                  <p className="subtitle-bold">{review.name}</p>
                  <p>{review.text}</p>
                  <hr />
                </>
              ))}
            </div>
          </div>
        </div>
        <div className="margin-top">
          <div
            className="home-date"
            style={{ padding: "20px 30px", width: 350 }}
          >
            <h4>Select dates</h4>
            <label className="bold">
              {" "}
              Start date
              <input className="small-input input" type="datetime-local" />
            </label>
            <label className="bold">
              {" "}
              End date
              <input className="small-input input" type="datetime-local" />
            </label>
            <label className="bold"> Amount of guests</label>
            <select>
              {guestsAmount.map((amount) => (
                <option value={amount}>{amount}</option>
              ))}
            </select>
            <a href="/booking-overview">
              {" "}
              <button className="wide margin-top">Select</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
