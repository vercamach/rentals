import React from "react";
import { Filter } from "./elements";

export default function HousesOverview(props) {
  function redirectUrl() {
    localStorage.getItem("email") === null
      ? props.history.push("/login")
      : props.history.push("/listed-home-detail");
  }

  const countryName = window.location.href.endsWith("denmark")
    ? "Denmark"
    : window.location.href.endsWith("norway")
    ? "Norway"
    : "Sweden";

  const houses = [
    {
      src: require("../imgs/img1.jpg"),
      city: "Trømso",
      rating: "4,65",
      title: "House in quiet neighbourhood"
    },
    {
      src: require("../imgs/img2.jpg"),
      city: "Visby",
      rating: "4,3",
      title: "Urban cabin with garden"
    },
    {
      src: require("../imgs/img3.jpg"),
      city: "Dragør",
      rating: "3,7",
      title: "Spacious house next to the sea"
    },
    {
      src: require("../imgs/img4.jpg"),
      city: "Helsingborg",
      rating: "4,43",
      title: "Cosy family home with 5 rooms"
    },
    {
      src: require("../imgs/img5.jpg"),
      city: "Lund",
      rating: "5",
      title: "Romantic flat with a view"
    },
    {
      src: require("../imgs/img6.jpg"),
      city: "Lilehammer",
      rating: "4,7",
      title: "Outstanding house on a hill"
    },
    {
      src: require("../imgs/img7.jpg"),
      city: "Næstved",
      rating: "3,8",
      title: "Charming apartment in the inner city"
    },
    {
      src: require("../imgs/img8.jpg"),
      city: "Skive",
      rating: "4,7",
      title: "Bright home in Skive"
    },
    {
      src: require("../imgs/img9.jpg"),
      city: "Flåm",
      rating: "5",
      title: "Modern apartment close to clubs"
    },
    {
      src: require("../imgs/img10.jpg"),
      city: "Borås",
      rating: "4,32",
      title: "Beautiful house with a sea view"
    },
    {
      src: require("../imgs/img11.jpg"),
      city: "Roskilde",
      rating: "3,98",
      title: "Newly built apartment in Roskilde"
    },
    {
      src: require("../imgs/img12.jpg"),
      city: "Narvik",
      rating: "4,8",
      title: "Village house with a garden"
    }
  ];
  return (
    <div style={{ width: "95vw", margin: " 50px auto", display: "flex" }}>
      <Filter />
      <div style={{ width: "75%" }}>
        <h3>{countryName}</h3>
        <div
          style={{
            display: "flex",
            flexFlow: "wrap",
            justifyContent: "space-between",
            margin: "auto 30px auto 0"
          }}
        >
          {houses.map((house) => (
            <div
              onClick={redirectUrl}
              style={{ overlay: "hidden", marginBottom: 50, cursor: "pointer" }}
            >
              <div>
                <img src={house.src} width="250px" height="170px" alt="img" />
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: 250,
                  marginTop: 8
                }}
              >
                <p style={{ fontSize: 13, marginBottom: 0, color: "	#808080" }}>
                  {house.city}
                </p>
                <p style={{ fontSize: 13, marginBottom: 0, color: "	#808080" }}>
                  {" "}
                  <img
                    src={require("../imgs/star-icon.png")}
                    alt="icon"
                    height="12px"
                    style={{ marginBottom: 5, marginRight: 5 }}
                  />
                  {house.rating}
                </p>
              </div>
              <p style={{ fontSize: 14 }}>{house.title}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
