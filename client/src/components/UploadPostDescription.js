import React from "react";
import Form from 'react-bootstrap/Form';
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import Dropzone from "react-dropzone";


export default function UploadPostDescripton(props) {
    const localStorageEmail = localStorage.getItem('email');
    localStorageEmail === null && props.history.push("/login");


    const [values, setValues] = React.useState({ userEmail: localStorageEmail })

    const [errorMessage, setErrorMessage] = React.useState()
    const countries = ["Denmark", "Sweden", "Norway"]
    return (
        <div>
            <Breadcrumb>
                <Breadcrumb.Item href="/facilities">Facilities</Breadcrumb.Item>
                <Breadcrumb.Item active>Description</Breadcrumb.Item>
            </Breadcrumb>
            <div className="div-gray" >

                <div className="form-wrapper">
                    <Form
                        onSubmit={async e => {
                            e.preventDefault();
                            const req = await fetch("http://localhost:8080/upload", {
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                method: "POST",
                                body: JSON.stringify(values)
                            });
                            if (req.status !== 201) {
                                const res = await req.json()
                                setErrorMessage(res.message)
                            } else {
                                window.location.href = "/my-homes";


                            }
                        }} action="http://localhost:8080/upload" method="POST"
                    >
                        <h1>Describe the home:</h1>
                        <p>Tell guests more about the home and add a picture</p>
                        <Form.Group >
                            <Form.Label className="bold">*Title</Form.Label>
                            <p className="form-description"> Provide a short title.</p>
                            <Form.Control type="text" required onChange={(e) => setValues({ ...values, title: e.target.value })} />
                        </Form.Group>
                        <div className="select-country">
                            <Form.Label className="bold">*Country</Form.Label>
                            <p className="form-description">Select a country.</p>
                            <select required onChange={(e) => setValues({ ...values, country: e.target.value })} >
                                <option disabled selected value="select one">Select one</option>
                                {countries.map((country, i) => <option key={i} value={country}>{country}</option>)}
                            </select>
                        </div>
                        <Form.Group >
                            <Form.Label className="bold">*Description</Form.Label>
                            <p className="form-description">Write a summary of your holiday home. You can also describe how will you communicate with guests.</p>
                            <Form.Control as="textarea" rows="4" required onChange={(e) => setValues({ ...values, description: e.target.value })} />
                        </Form.Group>

                        <Form.Group >
                            <Form.Label className="bold">Transport</Form.Label>
                            <p className="form-description">Write something about the transport options.</p>
                            <Form.Control as="textarea" rows="4" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label className="bold">Neighbourhood</Form.Label>
                            <p className="form-description">Share some information about your neighbourhood.</p>
                            <Form.Control as="textarea" rows="4" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label className="bold">*Image</Form.Label>
                            <p className="form-description">Place here and image of the holiday home.</p>
                            <Dropzone
                                multiple={false}
                                onDrop={acceptedFiles => {
                                    acceptedFiles.forEach(file => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(file);
                                        reader.onload = () => {
                                            setValues({
                                                ...values,
                                                img: reader.result
                                            });
                                        };
                                    });
                                }}
                            >
                                {({ getRootProps, getInputProps }) => {
                                    return (
                                        <div className="dropzone"
                                            {...getRootProps()}
                                        >
                                            {values && values.img ? (
                                                <img className="dropzone-img"
                                                    src={values.img}

                                                    alt="logo"
                                                />
                                            ) : (
                                                    <div style={{ margin: "0 auto" }}>
                                                        <img
                                                            style={{ width: 20, marginBottom: "-5px" }}
                                                            src={require("../imgs/upload-icon.svg")}
                                                            alt="logo"
                                                        />
                                                        <p
                                                            style={{
                                                                padding: 0,
                                                                marginLeft: 13,
                                                                marginBottom: 0,
                                                                color: "#C5D0DE",
                                                                textTransform: "uppercase",
                                                                fontSize: 11,
                                                                letterSpacing: 1,
                                                                display: "inline"
                                                            }}
                                                        >
                                                            Place your image here
                              </p>
                                                    </div>
                                                )}
                                            <input name="logo" {...getInputProps()} />
                                        </div>
                                    );
                                }}
                            </Dropzone>
                        </Form.Group>
                        <p className="error-message">{errorMessage}</p>
                        <button type="submit" className="wide ">Save</button>
                    </Form>
                </div>
            </div>
        </div >
    )
}