import React from "react";
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Form from 'react-bootstrap/Form';
import { OverviewBox } from "./elements";


export default function BookingOverview() {
    return (
        <div>
            <Breadcrumb>
                <Breadcrumb.Item active>Booking overview</Breadcrumb.Item>
            </Breadcrumb>
            <div className="single-house-container">
                <div className="margin-top" style={{ width: 500 }}>

                    <h2 style={{ fontWeight: 300 }}>Tell us more about yourself</h2>
                    <Form.Group >
                        <Form.Label className="bold">Who is coming?</Form.Label>
                        <p className="form-description">Let the owner know about the guests that are coming</p>
                        <Form.Control as="textarea" rows="3" />
                    </Form.Group>
                    <Form.Group >
                        <Form.Label className="bold">What is the purpose of your stay?</Form.Label>
                        <p className="form-description">Are you coming for a vacation, conference or for business?</p>
                        <Form.Control as="textarea" rows="3" />
                    </Form.Group>
                    <Form.Group >
                        <Form.Label className="bold">Say hi to your host</Form.Label>
                        <p className="form-description">Use this to send any extra information to the host</p>
                        <Form.Control as="textarea" rows="3" />
                    </Form.Group>



                </div>
                <div className="margin-top" style={{ width: 500, textAlign: "center" }}>
                    <OverviewBox />
                </div>
            </div>
            <div style={{ margin: "40px 0 20px 0", textAlign: "center" }}>
                <a href="/payment"> <button className="button-home">Continue</button></a>
            </div>
        </div >
    )
}