import React from 'react';
import Form from 'react-bootstrap/Form';
import "./style.css";
import { Checkbox } from "./elements";
import Breadcrumb from 'react-bootstrap/Breadcrumb'

export default function UploadPostFacilities(props) {
    const localStorageEmail = localStorage.getItem('email');
    localStorageEmail === null && props.history.push("/login");
    const housingTypes = ["House", "Flat", "Cottage", "Farm", "Summer house", "Room"];
    const roomsAmount = [1, 2, 3, 4, 5, 6];
    const guestsAmount = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const facilities = ["Heating", "Air condition", "Hair dryer", "Closet", "TV", "Wifi", "Washing machine", "Dryer", "Dishwasher", "Breakfast", "Fire extinguisher", "Fire detector", "First aid kit", "Parking", "Elevator", "Gym", "Swimming pool", "Whirpool"]
    return (
        <div>
            <Breadcrumb>
                <Breadcrumb.Item active >Facilities</Breadcrumb.Item>
            </Breadcrumb>
            <div className="div-gray" >
                <div className="form-wrapper">
                    <Form
                        onSubmit={e => {
                            e.preventDefault();
                            props.history.push("/description");
                        }}
                    >
                        <div>
                            <h1>Describe the home:</h1>
                            <p>In this section choose which facilities can guests expect</p>

                            <label className="bold">Housing type</label>
                            <select className="margin-bottom">
                                {housingTypes.map(type => <option value={type}>{type}</option>)}
                            </select>
                            <div className="flex">
                                <div >
                                    <label className="bold"> Amount of rooms</label>
                                    <select className="small-input" >
                                        {roomsAmount.map(amount => <option value={amount}>{amount}</option>)}
                                    </select>
                                </div>
                                <div className="margin-bottom">
                                    <label className="bold"> Amount of guests</label>
                                    <select className="small-input" >
                                        {guestsAmount.map(amount => <option value={amount}>{amount}</option>)}
                                    </select>
                                </div>
                            </div>
                            <div className="margin-bottom">
                                <label className="bold"> Facilities</label>
                                {facilities.map((name, i) => (
                                    <Checkbox
                                        key={i}
                                        name="facilities"
                                        value={i}
                                        label={name}
                                        style={{
                                            fontStyle: "italic"
                                        }}
                                    />
                                ))}
                            </div>
                        </div>
                        <button type="submit" className="wide" >Continue</button>
                    </Form>
                </div>
            </div>
        </div>
    )
};