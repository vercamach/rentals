import React from "react";
import { RenderSavedHome } from "./elements";

export default function Wishlist() {
    const savedHomes = [{
        title: "Cozy flat in the heart of Denmark",
        description: "Beautiful flat in the center of copenhagen. The flat is on the 4th floor with a view to the historical centrum. From the windows, you can see the parlament and the old stock exchange. The closes metro station is 5 minutes away, 15 minutes to train and 5 minutes to a closes bus. There are few supermarkets close by as well as a big shopping mall.",
        img: require("../imgs/denmark.jpg"),

    }, {
        title: "Beautiful house in Norway",
        description: "The house is in a private part of island with a view to the sea and a forest. There is a boat if you want to make trips to islands around or you can take a car and drive to the nearest city (20min). There is no public transport around.",
        img: require("../imgs/norway.jpg"),
    }, {
        title: "Romantic getaways in a cabin",
        description: "Small cabin a few minutes walking from a big lake. This place will be perfect if you want to escape the noise and enjoy romantic evenings, this place is perfect for you. You can get to the city by boat or by bus if you don't have a car and the closes small store is 30 minutes away walking.",
        img: require("../imgs/finland.jpg"),
    }, {
        title: "Modern apartment on Södermalm",
        description: "Newly built apartment on one of the most beautiful islands in Stockholm. There is a public transport in front of the building, there is also a free parking. If you want to go shopping, there is plenty of small local stores around and also a big shopping malls.",
        img: require("../imgs/sweden.jpg"),
    }]
    return (

        <div className="home-container">
            {savedHomes.map(home =>
                <div className="home-div">
                    <RenderSavedHome title={home.title} description={home.description} img={home.img} />
                    <div className="button-home-div">
                        <a href="/listed-home-detail">
                            <button className="button-home">Show more</button>
                        </a>
                    </div>
                </div>

            )}
        </div>


    )

}