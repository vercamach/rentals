import React from "react";
import Form from "react-bootstrap/Form";
import { NavLink } from "react-router-dom";

export default function Landing(props) {
  const peopleAmount = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const pickACountry = [
    {
      img: require("../imgs/copenhagen-landing.jpg"),
      name: "denmark",
      subtitle:
        "Denmark is not just a country full of beautiful architecture. Visit some of the worlds most famous restaurants and learn more about sutainable living."
    },
    {
      img: require("../imgs/norway-landing.jpg"),
      name: "norway",
      subtitle:
        "If you are interested in hiking through forests and fjords or want to experience midnight sun, Norway is the perfect choice for you."
    },
    {
      img: require("../imgs/stockholm-landing.jpg"),
      name: "sweden",
      subtitle:
        "Sweden is the perfect place if you love museums and diversity. You can enjoy the buzz of the cities as well as the quiet countryside."
    }
  ];
  return (
    <div>
      <div className="div-background-img">
        <div className="form-wrapper">
          <div>
            <div>
              <h1>Find a rental home </h1>
              <Form.Group>
                <Form.Label>Location</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
              <div className="flex">
                <Form.Group className="small-input">
                  <Form.Label>Arrival</Form.Label>
                  <Form.Control type="datetime-local" />
                </Form.Group>
                <Form.Group className="small-input">
                  <Form.Label>Departure</Form.Label>
                  <Form.Control type="datetime-local" />
                </Form.Group>
              </div>
              <div className="flex">
                <Form.Group className="small-input">
                  <Form.Label>Adults</Form.Label>
                  <select className="small-input">
                    {peopleAmount.map((amount, i) => (
                      <option value={amount} key={i}>
                        {amount}
                      </option>
                    ))}
                  </select>
                </Form.Group>
                <Form.Group className="small-input">
                  <Form.Label>Kids</Form.Label>
                  <select className="small-input">
                    <option>0</option>
                    {peopleAmount.map((amount, i) => (
                      <option value={amount} key={i}>
                        {amount}
                      </option>
                    ))}
                  </select>
                </Form.Group>
              </div>
              <NavLink className="buttonSearch" to="/houses-overview/houses">
                <button className="wide margin-top"> Search</button>
              </NavLink>
            </div>
          </div>
        </div>
      </div>
      <div style={{ width: "85%", margin: "auto", marginTop: 50 }}>
        <h3>Explore by country</h3>
        <div
          style={{ display: "flex", justifyContent: "space-between" }}
          className="margin-top"
        >
          {pickACountry.map((country, i) => (
            <div style={{ width: 335 }} key={i}>
              <a href={`/houses-overview/${country.name}`}>
                <div className="country">
                  <img src={country.img} alt="country img" />
                </div>
              </a>
              <p
                className="subtitle-bold"
                style={{ textTransform: "uppercase" }}
              >
                {country.name}
              </p>
              <p>{country.subtitle}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
