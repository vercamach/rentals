import React from "react";

export default function PurchaseConfirmation() {
  return (
    <div>
      <div style={{ textAlign: "center", marginTop: 150 }}>
        <h1
          style={{
            fontSize: 36,
            marginBottom: 15,
            fontWeight: 300
          }}
        >
          Thank You!
        </h1>
        <p className="margin-bottom">
          Your reservation has been successfully sent to the administrator.{" "}
        </p>

        <p className="margin-bottom"> Your reservation ID is RM1557134393</p>
        <p className="margin-bottom">
          {" "}
          If you have any questions, please contact us.
        </p>

        <button className="button-home">Send receipt</button>
      </div>
    </div>
  );
}
