import React from "react";
import Form from "react-bootstrap/Form";
import "./style.css";

export default function Login(props) {
  const [values, setValues] = React.useState();
  const [errorMessage, setErrorMessage] = React.useState();

  return (
    <div className="div-background-img">
      <div className="form-wrapper">
        <Form
          onSubmit={async (e) => {
            e.preventDefault();
            const req = await fetch("http://localhost:8080/users/login", {
              headers: {
                "Content-Type": "application/json"
              },
              method: "POST",
              body: JSON.stringify(values)
            });
            if (req.status !== 200) {
              const res = await req.json();
              setErrorMessage(res.message);
            } else {
              localStorage.setItem("email", values.email);
              window.location.href = "/";
            }
          }}
          action="http://localhost:8080/users/login"
          method="POST"
        >
          <div>
            <h1>Log in</h1>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                required
                onChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                required
                onChange={(e) =>
                  setValues({ ...values, password: e.target.value })
                }
              />
            </Form.Group>
            <button className="wide margin">Log in</button>
            <p>
              If you don't have an account, you can{" "}
              <a href="/signup">SIGN UP</a>
            </p>
            <p className="error-message">{errorMessage}</p>
          </div>
        </Form>
      </div>
    </div>
  );
}
