import React from "react";
import { OverviewBox } from "./elements";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export default function Payment() {
  const years = [
    2019,
    2020,
    2021,
    2022,
    2023,
    2024,
    2025,
    2026,
    2027,
    2028,
    2029,
    2030
  ];
  const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item href="/booking-overview">
          Booking overview
        </Breadcrumb.Item>
        <Breadcrumb.Item active>Payment</Breadcrumb.Item>
      </Breadcrumb>
      <div className="single-house-container">
        <div className="margin-top" style={{ width: 500 }}>
          <h2 style={{ fontWeight: 300 }}>Confirm and pay</h2>
          <p>Pay with:</p>

          <div className="payment-option-box">
            <button className="payment-option">
              {" "}
              <img
                className="logo-payment"
                src={require("../imgs/mastercard.png")}
                alt="mastercard"
              />
              **** **** **** 3165
            </button>
            <button className="payment-option">
              <img
                className="logo-payment"
                src={require("../imgs/visa.png")}
                alt="visa"
              />
              **** **** **** 6254
            </button>
            <button className="payment-option">
              <img
                className="logo-payment"
                src={require("../imgs/ame-ex.png")}
                alt="am-ex"
              />
              **** **** **** 3902
            </button>
            <p>or:</p>
            <div className="card">
              <div className="card-header">
                <strong>Credit Card</strong>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                      <label for="name">Name</label>
                      <input
                        className="form-control"
                        id="name"
                        type="text"
                        placeholder="Enter your name"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                      <label for="ccnumber">Credit Card Number</label>
                      <div className="input-group">
                        <input
                          className="form-control"
                          type="text"
                          placeholder="0000 0000 0000 0000"
                          autocomplete="email"
                        />
                        <div className="input-group-append">
                          <span className="input-group-text">
                            <i className="mdi mdi-credit-card"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="form-group col-sm-4">
                    <label for="ccmonth">Month</label>
                    <select className="form-control" id="ccmonth">
                      {months.map((month) => (
                        <option>{month}</option>
                      ))}
                    </select>
                  </div>
                  <div className="form-group col-sm-4">
                    <label for="ccyear">Year</label>
                    <select className="form-control" id="ccyear">
                      {years.map((year) => (
                        <option>{year}</option>
                      ))}
                    </select>
                  </div>
                  <div className="col-sm-4">
                    <div className="form-group">
                      <label for="cvv">CVV/CVC</label>
                      <input
                        className="form-control"
                        id="cvv"
                        type="text"
                        placeholder="123"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div></div>
        </div>
        <div className="margin-top" style={{ width: 500, textAlign: "center" }}>
          <OverviewBox />

          <div style={{ textAlign: "center", width: 350, marginTop: 100 }}>
            <a href="/purchase-confirmation">
              {" "}
              <button className="button-home">Continue</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
