import React from "react";
import { RenderSavedHome } from "./elements";

export default function ListedHomes(props) {
  const localStorageEmail = localStorage.getItem("email");
  localStorageEmail === null && props.history.push("/login");

  const [homes, setHomes] = React.useState([]);
  React.useEffect(() => {
    async function fetchApi() {
      let req = await fetch("http://localhost:8080/listed-homes");
      req.json().then((res) => {
        setHomes(res);
      });
    }
    fetchApi();
  }, []);
  const filteredHomes = homes.filter((home) => {
    return home.userEmail === localStorageEmail;
  });
  return (
    <div className="home-container">
      {filteredHomes.length === 0
        ? "You didn't list any homes yet."
        : filteredHomes.map((home) => (
            <div className="home-div">
              <RenderSavedHome
                title={home.title}
                description={home.description}
                img={home.img}
              />
              <div className="button-home-div">
                <a href="/listed-home-detail/">
                  <button className="button-home">Show more</button>
                </a>
              </div>
            </div>
          ))}
    </div>
  );
}
