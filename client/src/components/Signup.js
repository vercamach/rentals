import React from "react";
import Form from "react-bootstrap/Form";

export default function Signup(props) {
  const [values, setValues] = React.useState();
  const [errorMessage, setErrorMessage] = React.useState();
  return (
    <div className="div-background-img">
      <div className="form-wrapper">
        <Form
          onSubmit={async (e) => {
            e.preventDefault();

            const req = await fetch("http://localhost:8080/users/register", {
              headers: {
                "Content-Type": "application/json"
              },
              method: "POST",
              body: JSON.stringify(values)
            });
            if (req.status !== 201) {
              const res = await req.json();
              setErrorMessage(res.message);
            } else {
              props.history.push("/login");
            }
          }}
          action="http://localhost:8080/users/register"
          method="POST"
        >
          <div>
            <h1>Sign up</h1>
            <p>Please fill in this form to create an account.</p>

            <Form.Group controlId="formBasicText">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                required
                onChange={(e) =>
                  setValues({ ...values, username: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                required
                onChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                required
                onChange={(e) =>
                  setValues({ ...values, password: e.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formRepeatPassword">
              <Form.Label> Repeat password</Form.Label>
              <Form.Control
                type="password"
                required
                onChange={(e) =>
                  setValues({ ...values, repeatPassword: e.target.value })
                }
              />
            </Form.Group>

            <button type="submit" className="wide ">
              Register
            </button>
            <p className="error-message">{errorMessage}</p>
            <p>
              Already have an account? Go <a href="/login">LOG IN</a>
            </p>
          </div>
        </Form>
      </div>
    </div>
  );
}
