import React from 'react';

export class Checkbox extends React.Component {
    static defaultProps = {
        defaultChecked: false
    };
    state = {
        checked: this.props.defaultChecked
    };
    render() {
        const { label, id, onChange } = this.props;
        return (
            <div
                style={
                    {
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                        margin: "13px 0",
                    }
                }
                onClick={() => {
                    this.setState(
                        ({ checked }) => ({ checked: !checked }),
                        () => {
                            if (onChange) onChange(this.state.checked);
                        }
                    );
                }}
            >
                <input
                    id={id}
                    type="checkbox"
                    checked={this.state.checked}
                />
                {
                    label && (
                        <label

                            style={{
                                marginBottom: 0,
                                userSelect: "none",
                                paddingLeft: 5
                            }}
                        >
                            {label}
                        </label>
                    )
                }
            </div >
        );
    }
}

export const RenderSavedHome = ({ title, description, img }) => {
    return (
        <div >
            <h2 className="title-home">{title}</h2>
            <p className="subtitle-home">{description}</p>
            <div className="home-img-section">
                <img className="img-home" src={img} alt="img" />
            </div>

        </div>
    )
}

export const Modal = ({
    children,
    isOpen,
    onClose,
    modalStyle = {},
    style = {}
}: {
        children: React.Element<*>,
        isOpen: boolean,
        onClose: (e: Event) => void,
        modalStyle?: { [string]: any },
        style?: { [string]: any }
    }) => {
    return (
        <div
            role="dialog"
            aria-hidden={!isOpen}
            style={{
                zIndex: 101,
                top: 0,
                display: "flex",
                flexDirection: "column",
                transition: "all 200ms",

                ...(isOpen
                    ? {
                        opacity: 1,
                        visibility: "visible"
                    }
                    : {
                        opacity: 0,
                        visibility: "hidden"
                    })
            }}
        >
            <div
                onClick={onClose}
                style={{
                    position: "fixed",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: "100%",
                    backgroundColor: "#10234D",
                    opacity: 0.3,
                    zIndex: 101
                }}
            />
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    position: "fixed",
                    top: "53%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    transition: "500ms opacity",
                    zIndex: 1000,
                    height: "100%",
                    maxHeight: "700px",
                    ...style
                }}
            >
                <div
                    style={{
                        display: "flex",
                        minHeight: 50,
                        padding: 10
                    }}
                >
                    <div
                        style={{
                            background: "white",
                            borderRadius: 5,
                            boxShadow: "3px 3px 3px #878787",
                            width: "100vw",
                            maxWidth: 620,
                            overflowY: "scroll",
                            ...modalStyle
                        }}
                    >
                        <div
                            data-testid="close"
                            onClick={onClose}
                            style={{
                                position: "absolute",
                                right: 15,
                                top: 15,
                                cursor: "pointer",
                                zIndex: 1000,
                                width: 40,
                                height: 40
                            }}
                        >
                            <img src={require("../imgs/cancel-icon.png")} width="20px" alt="img" />
                        </div>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
};


export const OverviewBox = () => {
    return (
        <div className="home-date" style={{ padding: "20px 30px", width: 350 }}>
            <h4 style={{ fontSize: 19 }}>Charming house next to a nature</h4>
            <img src={require("../imgs/denmark.jpg")} width="80%" style={{ margin: "auto" }} alt="denmark-img" />
            <div className="overview-div" >
                <p className="light-text"><img src={require("../imgs/star-icon.png")} alt="icon" height="15px" style={{ marginBottom: 5 }} /> 4,76</p>
                <p className="light-text">435 reviews</p>
            </div>
            <hr />
            <div className="overview-div">
                <p className="light-text"><img src={require("../imgs/guest-icon.png")} alt="icon" height="15px" style={{ marginBottom: 5, marginRight: 5 }} />3 guests</p>
            </div>
            <div className="overview-div">
                <p className="light-text"><img src={require("../imgs/calendar-icon.png")} alt="icon" height="15px" style={{ marginBottom: 5, marginRight: 5 }} />Arrival</p>
                <p className="light-text">27.dec, 15:00</p>
            </div>
            <div className="overview-div">
                <p className="light-text"><img src={require("../imgs/calendar-icon.png")} alt="icon" height="15px" style={{ marginBottom: 5, marginRight: 5 }} />Departure</p>
                <p className="light-text">3.jan, 10:00</p>
            </div>
            <hr />
            <div style={{ width: "78%", margin: "auto" }} >
                <span className="flex-container justify-between">
                    <p className="light-text">7 x 156,00 dkk</p>
                    <p className="light-text">10192 dkk</p>
                </span>
                <span className="flex-container justify-between">
                    <p className="light-text">Cleaning: </p>
                    <p className="light-text">100 dkk</p>
                </span>
                <span className="flex-container justify-between">
                    <p className="light-text">Service charge:  </p>
                    <p className="light-text">100 dkk</p>
                </span>
                <hr />
                <span className="flex-container justify-between">
                    <p className="light-text">Total:  </p>
                    <p className="light-text">12192 dkk</p>
                </span>
            </div>
        </div>
    )
}

export const Filter = () => {

    const housingTypes = ["House", "Flat", "Cottage", "Farm", "Summer house", "Room"];
    const facilities = ["Heating", "Air condition", "Wifi", "Washing machine", "Dishwasher", "Parking", "Pets allowed", "Family friendly"]
    const pricePerNight = ["100 - 300", "300 - 500", "500 - 700", "700 - 900", "900 - 1100", "above 1100"]
    return (
        <div style={{ margin: "0 40px" }}>

            <p style={{ fontSize: 18 }}>FILTER</p>


            <div>
                <li >
                    <p className="filter-bold">Type of house</p>
                    <hr />
                    <div >
                        {housingTypes.map(type =>
                            <div><input type="checkbox" /><label className="form-description margin-left">{type}</label></div>
                        )}
                    </div>
                </li>
                <br />
                <li>
                    <p className="filter-bold">Facilities</p>
                    <hr />
                    <div >
                        {facilities.map(facility =>
                            <div><input type="checkbox" /><label className="form-description margin-left">{facility}</label></div>
                        )}
                    </div>
                </li>
                <br />
                <li>
                    <p className="filter-bold">Price per night</p>
                    <hr />
                    <div >
                        {pricePerNight.map(price =>
                            <div><input type="checkbox" /><label className="form-description margin-left">{price}</label></div>
                        )}
                    </div>
                </li>
                <br />
            </div>
        </div >
    )
}
