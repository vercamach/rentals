const MongoClient = require("mongodb").MongoClient;
const session = require('express-session');
const url = "mongodb://localhost:27017";
const dbname = "holidayhomes";
const usersCollectionName = "users";
const housesCollectionName = "houses";
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');

var multer = require('multer');
var upload = multer({ dest: 'uploads/' });
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // allow domain to make request
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.json());

app.post('/users/register', async (req, res) => {
    const username = req.body.username;
    if (!username) {
        res.status(400).json({ message: "Username is missing" });
        return;
    }
    if (username.length < 5) {
        res.status(400).json({ message: "Username has to have at least 5 characters" });
        return;
    }
    const email = req.body.email;
    if (!email) {
        res.status(400).json({ message: "Email is missing" });
        return;
    }
    const password = req.body.password;
    if (!password) {
        res.status(400).json({ message: "Password is missing" });
        return;
    }

    if (password.length < 5) {
        res.status(400).json({ message: "Password has to have at least 5 characters" });
        return;
    }
    const repeatPassword = req.body.repeatPassword;
    if (!repeatPassword) {
        res.status(400).json({ message: "Repeat password missing" });
        return;
    }
    if (password !== repeatPassword) {
        res.status(400).json({ message: "Passwords don't match" });
        return;
    }

    MongoClient.connect(url, (error, client) => {
        if (error) {
            console.log(error)
        }
        const db = client.db(dbname);
        const usersCollection = db.collection(usersCollectionName);
        usersCollection.findOne({ username: username }, function (err, username) {
            if (err) {
                console.log(err);
            }
            if (username) {
                res.status(400).json({ message: "Username exists" });
                return;
            }
        });

        usersCollection.findOne({ email: email }, function (err, email) {
            if (err) {
                console.log(err)
            }
            if (email) {
                res.status(400).json({ message: "Email exists" });
                return;
            }
        });

        bcrypt.hash(req.body.password, 10, async function (err, hash) {
            const newUser = {
                username: req.body.username,
                email: req.body.email,
                password: hash
            }
            usersCollection.insertOne(
                newUser, function (err, result) {
                    if (err) {
                        console.log(err)
                    }
                    if (result) {
                        res.status(201).json({ message: 'You are signed up' });
                        return;
                    } else {
                        res.status(400).json({ message: 'Something went wrong' });
                        return;
                    }
                });
        });
    });
});

app.post('/users/login', async (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    MongoClient.connect(url, (error, client) => {
        const db = client.db(dbname);
        const usersCollection = db.collection(usersCollectionName);
        usersCollection.findOne({ email: email }, function (erro, user) {
            if (erro) {
                console.log(error)
            }
            if (user) {
                const userId = user._id
                bcrypt.compare(password, user.password, function (err, response) {
                    if (err) {
                        return res.status(500).json({ message: 'Problem hashing' });
                    }
                    if (response === true) {
                        return res.status(200).json({ message: 'You are logged in', userId });
                    } else {
                        return res.status(400).json({ message: 'Password  is wrong' });
                    }
                });
            }
            else {
                res.status(400).json({ message: "User doesn't exist" });
            }
        });
    });
});

app.post('/upload', upload.single('message'), (req, res) => {
    const userEmail = req.body.userEmail;
    if (!userEmail) {
        res.status(400).json({ message: "User is not authorized" });
    }
    const title = req.body.title;
    if (!title) {
        res.status(400).json({ message: "Please provide a title" });
    }
    const country = req.body.country;
    if (!country) {
        res.status(400).json({ message: "Please provide a country" });
    }
    const description = req.body.description;
    if (!description) {
        res.status(400).json({ message: "Please provide a description" });
    }
    const img = req.body.img;
    if (!img) {
        res.status(400).json({ message: "Please provide an image" });
    }
    const newHome = {
        userEmail: userEmail,
        title: title,
        country: country,
        description: description,
        img: img
    }
    MongoClient.connect(url, (error, client) => {
        const db = client.db(dbname);
        const housesCollection = db.collection(housesCollectionName);
        housesCollection.insertOne(
            newHome, function (err, result) {
                if (err) {
                    console.log(err)
                }
                if (result) {
                    return res.status(201).json({ message: 'Your home was saved' });
                } else {
                    return res.status(400).json({ message: 'Something went wrong' });
                }
            });
    });
})

app.get('/listed-homes', (req, res) => {
    MongoClient.connect(url, (error, client) => {
        const db = client.db(dbname);
        const housesCollection = db.collection(housesCollectionName);

        housesCollection.find().toArray((error, houses) => {
            if (error) {
                console.log(error)
            }
            if (houses) {
                res.send(houses)
            }
        });
    })
})

app.listen(8080, (error) => {
    if (error) {
        console.log(error);
    }
    console.log('running on 8080')
});